CREATE TABLE IF NOT EXISTS categories (
    id uuid primary key,
    name varchar(50) unique,    
    parent_id uuid, -- Reference bolsa, boshida birinchi category create bolmirkan
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS products (
    id uuid primary key,
    name varchar(100) unique,
    category_id uuid references categories(id),
    barcode varchar(10) unique,
    price float,
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0
);