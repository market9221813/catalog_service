package postgres

import (
	pb "catalog_service/genproto/catalog_service_protos"
	"catalog_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type categoryRepo struct {
	DB *pgxpool.Pool
}

func NewCategoryRepo (db *pgxpool.Pool) storage.ICategoryStorage{
	return &categoryRepo{
		DB: db,
	}
}

func (c *categoryRepo) Create(ctx context.Context,createCategory *pb.CreateCategoryRequest) (*pb.Category, error) {
	category := pb.Category{}

	query := `INSERT INTO categories (id,name, parent_id)
			values ($1, $2, $3) 
		returning id, name, parent_id, created_at::text `

	uid := uuid.New()

	err := c.DB.QueryRow(ctx, query, uid, createCategory.GetName(), createCategory.GetParentId()).Scan(
		&category.Id,
		&category.Name,
		&category.ParentId,
		&category.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating category!", err.Error())
		return &pb.Category{},err
	}

	return &category, nil
}

func (c *categoryRepo) Get(ctx context.Context,pKey *pb.CategoryPrimaryKey) (*pb.Category, error) {
	category := pb.Category{}

	query := `SELECT id, name, parent_id, created_at::text, updated_at::text from categories
				WHERE id = $1 AND deleted_at = 0 `
	err := c.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&category.Id,
		&category.Name,
		&category.ParentId,
		&category.CreatedAt,
		&category.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting category!", err.Error())
		return &pb.Category{}, err
	}
	return &category, nil
}

func (c *categoryRepo) GetList(ctx context.Context, req *pb.GetCategoryListRequest) (*pb.CategoriesResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		categories = pb.CategoriesResponse{}
	)

	countQuery := `SELECT count(1) from categories where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND name ilike '%%%s%%' `, search)
	}

	err := c.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of categories!", err.Error())
		return &pb.CategoriesResponse{},err
	}

	query := `SELECT id, name, parent_id, created_at::text, updated_at::text from categories 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND name ilike '%%%s%%' `, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := c.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.CategoriesResponse{}, err
	}

	for rows.Next() {
		category := pb.Category{}

		err := rows.Scan(
			&category.Id,
			&category.Name,
			&category.ParentId,
			&category.CreatedAt,
			&category.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning categories!",err.Error())
			return &pb.CategoriesResponse{},err
		}

		categories.Categories = append(categories.Categories, &category)
	}

	categories.Count = count

	return &categories, nil
}

func (c *categoryRepo) Update(ctx context.Context, updCategory *pb.Category) (*pb.Category, error) {
	category := pb.Category{}

	query := `UPDATE categories SET name = $1, parent_id = $2, updated_at = NOW() 
			WHERE id = $3 AND deleted_at = 0 
		returning id, name, parent_id, updated_at::text `
	err := c.DB.QueryRow(ctx, query, updCategory.GetName(),updCategory.GetParentId(),updCategory.GetId()).Scan(
		&category.Id,
		&category.Name,
		&category.ParentId,
		&category.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating categories!", err.Error())
		return &pb.Category{}, err
	}

	return &category, nil

}

func (c *categoryRepo) Delete(ctx context.Context, pKey *pb.CategoryPrimaryKey) (error)  {
	query := `UPDATE categories SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := c.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting category!",err.Error())
		return err
	}
	return nil
}