package postgres

import (
	pb "catalog_service/genproto/catalog_service_protos"
	"catalog_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type productRepo struct {
	DB *pgxpool.Pool
}

func NewProductRepo (db *pgxpool.Pool) storage.IProductStorage{
	return &productRepo{
		DB: db,
	}
}

func (p *productRepo) Create(ctx context.Context,createProduct *pb.CreateProductRequest) (*pb.Product, error) {
	product := pb.Product{}

	query := `INSERT INTO products (id,name, category_id, barcode, price)
			values ($1, $2, $3, $4, $5) 
		returning id, name, category_id, barcode, created_at::text `

	uid := uuid.New()

	err := p.DB.QueryRow(ctx, query, 
		uid, 
		createProduct.GetName(), 
		createProduct.GetCategoryId(),
		createProduct.GetBarcode(),
		createProduct.GetPrice(),
		).Scan(
		&product.Id,
		&product.Name,
		&product.CategoryId,
		&product.Barcode,
		&product.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating Product!", err.Error())
		return &pb.Product{},err
	}

	return &product, nil
}

func (p *productRepo) Get(ctx context.Context,pKey *pb.ProductPrimaryKey) (*pb.Product, error) {
	product := pb.Product{}

	query := `SELECT id, name, category_id, barcode, price, created_at::text, updated_at::text from products
				WHERE id = $1 AND deleted_at = 0 `
	err := p.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&product.Id,
		&product.Name,
		&product.CategoryId,
		&product.Barcode,
		&product.Price,
		&product.CreatedAt,
		&product.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Product!", err.Error())
		return &pb.Product{}, err
	}
	return &product, nil
}

func (p *productRepo) GetList(ctx context.Context, req *pb.GetProductListRequest) (*pb.ProductsResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		products = pb.ProductsResponse{}
	)

	countQuery := `SELECT count(1) from products where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND name ilike '%%%s%%' OR barcode ilike '%%%s%%' `, search, search)
	}

	err := p.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of products!", err.Error())
		return &pb.ProductsResponse{},err
	}

	query := `SELECT id, name, category_id, barcode, price, created_at::text, updated_at::text from products 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND name ilike '%%%s%%' OR barcode ilike '%%%s%%' `, search, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := p.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.ProductsResponse{}, err
	}

	for rows.Next() {
		product := pb.Product{}

		err := rows.Scan(
			&product.Id,
			&product.Name,
			&product.CategoryId,
			&product.Barcode,
			&product.Price,
			&product.CreatedAt,
			&product.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning products!",err.Error())
			return &pb.ProductsResponse{},err
		}

		products.Products = append(products.Products, &product)
	}

	products.Count = count

	return &products, nil
}

func (p *productRepo) Update(ctx context.Context, updProduct *pb.Product) (*pb.Product, error) {
	product := pb.Product{}

	query := `UPDATE products SET name = $1, category_id = $2, barcode = $3, price = $4, updated_at = NOW() 
			WHERE id = $5 AND deleted_at = 0 
		returning id, name, category_id, barcode, price, updated_at::text `
	err := p.DB.QueryRow(ctx, query, 
		updProduct.GetName(),
		updProduct.GetCategoryId(),
		updProduct.GetBarcode(),
		updProduct.GetPrice(),
		updProduct.GetId(),
		).Scan(
		&product.Id,
		&product.Name,
		&product.CategoryId,
		&product.Barcode,
		&product.Price,
		&product.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating products!", err.Error())
		return &pb.Product{}, err
	}

	return &product, nil

}

func (p *productRepo) Delete(ctx context.Context, pKey *pb.ProductPrimaryKey) (error)  {
	query := `UPDATE products SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := p.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting product!",err.Error())
		return err
	}
	return nil
}

func (p *productRepo) GetPriceByBarcode(ctx context.Context, pKey *pb.ProductBarcodePrimaryKey) (*pb.ProductPriceResponse, error) {
	productPriceResp := pb.ProductPriceResponse{}
	query := `SELECT price from products where barcode = $1`

	err := p.DB.QueryRow(ctx,query,pKey.Barcode).Scan(
		&productPriceResp.Price,
	)
	if err != nil{
		fmt.Println("error while getting product price by barcode!", err.Error())
		return &pb.ProductPriceResponse{}, err
	}

	return &productPriceResp, nil
}

func (p *productRepo) UpdatePrice(ctx context.Context, req *pb.UpdateProductPriceRequest) (error) {
	query := `UPDATE products SET price = $1 where barcode = $2`

	_, err := p.DB.Exec(ctx,query, req.GetPrice(),req.GetBarcode())
	if err != nil{
		fmt.Println("error while updating product price!", err.Error())
		return err
	}
	return nil
}

func (p *productRepo) GetNameByBarcode(ctx context.Context, pKey *pb.ProductBarcodePrimaryKey) (*pb.ProductNameResponse,error) {
	query := `SELECT name from products where barcode = $1`

	productName := pb.ProductNameResponse{}

	err := p.DB.QueryRow(ctx, query, pKey.Barcode).Scan(
		&productName.Name,
	)
	if err != nil{
		fmt.Println("error while getting product name!",err.Error())
		return &pb.ProductNameResponse{},err
	}
	return &productName, nil
}