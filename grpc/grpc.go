package grpc

import (
	pb "catalog_service/genproto/catalog_service_protos"
	"catalog_service/grpc/client"
	"catalog_service/service"
	"catalog_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer (strg storage.IStorage, services client.IServiceManager)*grpc.Server{
	grpcServer := grpc.NewServer()
	
	pb.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(strg, services))
	pb.RegisterProductServiceServer(grpcServer, service.NewProductService(strg, services))
	reflection.Register(grpcServer)

	return grpcServer
}