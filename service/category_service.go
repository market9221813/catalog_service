package service

import (
	pb "catalog_service/genproto/catalog_service_protos"
	"catalog_service/grpc/client"
	"catalog_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedCategoryServiceServer
}

func NewCategoryService (storage storage.IStorage, services client.IServiceManager) *categoryService{
	return &categoryService{
		storage: storage,
		services: services,
	}
}

func (c *categoryService) Create(ctx context.Context, req *pb.CreateCategoryRequest) (*pb.Category,error) {
	category, err := c.storage.Category().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating category!", err.Error())
		return &pb.Category{},err
	}
	return category, nil
}

func (c *categoryService) Get(ctx context.Context, req *pb.CategoryPrimaryKey) (*pb.Category,error) {
	category, err := c.storage.Category().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting category!", err.Error())
		return &pb.Category{},err
	}
	return category, nil
}

func (c *categoryService) GetList(ctx context.Context, req *pb.GetCategoryListRequest) (*pb.CategoriesResponse,error) {
	categories, err := c.storage.Category().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting categories list!",err.Error())
		return &pb.CategoriesResponse{},err
	}
	return categories, nil
}

func (c *categoryService) Update(ctx context.Context, req *pb.Category) (*pb.Category, error) {
	category, err := c.storage.Category().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating category!",err.Error())
		return &pb.Category{}, err
	}
	return category, nil
}

func (c *categoryService) Delete(ctx context.Context, req *pb.CategoryPrimaryKey) (*emptypb.Empty,error) {
	err := c.storage.Category().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting category!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}