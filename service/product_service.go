package service

import (
	pb "catalog_service/genproto/catalog_service_protos"
	"catalog_service/grpc/client"
	"catalog_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type productService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedProductServiceServer
}

func NewProductService (storage storage.IStorage, services client.IServiceManager) *productService{
	return &productService{
		storage: storage,
		services: services,
	}
}

func (p *productService) Create(ctx context.Context, req *pb.CreateProductRequest) (*pb.Product,error) {
	product, err := p.storage.Product().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating product!", err.Error())
		return &pb.Product{},err
	}
	return product, nil
}

func (p *productService) Get(ctx context.Context, req *pb.ProductPrimaryKey) (*pb.Product,error) {
	product, err := p.storage.Product().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting product!", err.Error())
		return &pb.Product{},err
	}
	return product, nil
}

func (p *productService) GetList(ctx context.Context, req *pb.GetProductListRequest) (*pb.ProductsResponse,error) {
	products, err := p.storage.Product().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting products list!",err.Error())
		return &pb.ProductsResponse{},err
	}
	return products, nil
}

func (p *productService) Update(ctx context.Context, req *pb.Product) (*pb.Product, error) {
	product, err := p.storage.Product().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating Product!",err.Error())
		return &pb.Product{}, err
	}
	return product, nil
}

func (p *productService) Delete(ctx context.Context, req *pb.ProductPrimaryKey) (*emptypb.Empty,error) {
	err := p.storage.Product().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting product!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}

func (p *productService) GetPriceByBarcode(ctx context.Context, req *pb.ProductBarcodePrimaryKey)(*pb.ProductPriceResponse, error){
	productPriceResp, err := p.storage.Product().GetPriceByBarcode(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting product price by barcode!",err.Error())
		return &pb.ProductPriceResponse{}, err
	}

	return productPriceResp, nil
}

func (p *productService) UpdateProductPrice(ctx context.Context, req *pb.UpdateProductPriceRequest) (*emptypb.Empty, error) {
	err := p.storage.Product().UpdatePrice(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while updating product price!", err.Error())
		return nil, err
	}

	return nil, nil
}

func (p *productService) GetProductNameByBarcode(ctx context.Context, req *pb.ProductBarcodePrimaryKey) (*pb.ProductNameResponse, error) {
	productName,err := p.storage.Product().GetNameByBarcode(ctx,req)
	if err != nil{
		fmt.Println("Err in service, while getting product name!",err.Error())
		return &pb.ProductNameResponse{},err
	}
	return productName, nil
}